﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using AltUnityTester.AltUnityDriver.UnityStruct;
using Multiverse.Automation.Model;
using Multiverse.Automation.Questions;
using ScreenPlay;
using System;
using System.Threading;

namespace Multiverse.Automation.Commands {
    public sealed class TapCommand : ICommand {
        private readonly string buttonName;
        private readonly TimeSpan animationButton = TimeSpan.FromMilliseconds(500);

        public TapCommand(Button? button = null) {
            buttonName = button?.ToDescriptionString();
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();

            var position = GetPosition(actor, altUnityDriver);

            altUnityDriver.HoldButtonAndWait(position, Input.TapThresholdSecond);
        }

        private AltUnityVector2 GetPosition(IActor actor, AltUnityDriver altUnityDriver) {
            if (string.IsNullOrWhiteSpace(buttonName)) {
                return actor.AskFor(new CenterScreenPositionQuestion());
            }

            var buttonObj = altUnityDriver.WaitForObject(By.PATH, $"//*[contains(@name,{ buttonName })]");
            Thread.Sleep(animationButton);
            return buttonObj.getScreenPosition();
        }
    }
}