﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;
using System;
using System.Linq;

namespace Multiverse.Automation.Commands {
    public sealed class TurnOffCameraTemporarilyCommand : ICommand {
        private readonly Action callback;

        public TurnOffCameraTemporarilyCommand(Action callback) {
            this.callback = callback ?? throw new ArgumentNullException(nameof(callback));
        }

        // Workaround to get the right position on Story scene by disabling camera of the scene
        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            const string component = "UnityEngine.GameObject";
            const string method = "SetActive";
            const string assembly = "UnityEngine.CoreModule";

            var firstCamera = altUnityDriver.FindObjects(By.NAME, "Camera").First();
            // Disable camera
            firstCamera.CallComponentMethod(component, method, "false", "bool", assembly);

            try {
                callback();
            } finally {
                // Enable camera
                firstCamera.CallComponentMethod(component, method, "true", "bool", assembly);
            }
        }
    }
}