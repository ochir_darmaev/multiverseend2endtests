﻿using AltUnityTester.AltUnityDriver;
using ScreenPlay;

namespace Multiverse.Automation.Commands {
    public sealed class ScreenshotPNGCommand : ICommand {
        private readonly string path;

        public ScreenshotPNGCommand(string path) {
            this.path = path;
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.GetPNGScreenshot(path);
        }
    }
}