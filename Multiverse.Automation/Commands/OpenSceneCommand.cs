﻿using AltUnityTester.AltUnityDriver;
using FluentAssertions;
using Multiverse.Automation.Model;
using Newtonsoft.Json;
using ScreenPlay;
using System;

namespace Multiverse.Automation.Commands {
    public sealed class OpenScene : ICommand {
        private readonly TimeSpan timeout;
        private readonly string sceneName;

        public OpenScene(SceneName sceneName, TimeSpan timeout) {
            this.timeout = timeout;
            this.sceneName = sceneName.ToDescriptionString();
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();

            if (IsSameScene(altUnityDriver)) {
                return;
            }

            // todo AutomationTest should be encapsulated
            var response = altUnityDriver.CallStaticMethods("WhiteSharx.Mars.Automation.AutomationTest", "OpenScene", sceneName);
            var result = JsonConvert.DeserializeObject<Result>(response);
            result.Success.Should().BeTrue();
            result.ErrorMessage.Should().BeNull();

            altUnityDriver.WaitForCurrentSceneToBe(sceneName, timeout.TotalSeconds);
        }

        private bool IsSameScene(AltUnityDriver altUnityDriver) {
            var currentScene = altUnityDriver.GetCurrentScene();

            return sceneName == currentScene;
        }
    }
}