﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.AltUnity;
using Multiverse.Automation.Model;
using ScreenPlay;
using System;
using System.Linq;
using By = AltUnityTester.AltUnityDriver.Commands.FindObjects.By;

namespace Multiverse.Automation.Commands {
    public sealed class OpenMobileAppCommand : ICommand {
        private readonly string appName;
        public const int MaxRepeatFind = 50;
        public OpenMobileAppCommand(PhoneMenu appName) {
            this.appName = appName.ToDescriptionString();
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            var textObject = WaitText(altUnityDriver, "Phone", appName);
            altUnityDriver.HoldButtonAndWait(textObject.getScreenPosition(), Input.TapThresholdSecond);
        }

        public AltUnityObject WaitText(AltUnityDriver altUnityDriver, string component, string text) {
            for (int i = 0; i < MaxRepeatFind; i++) {
                var textObjects = altUnityDriver.FindObjectsWhichContain(By.PATH, $"//{component}//Text");

                var textObject = textObjects?
                    .Where(x => x.GetText() == text)
                    .ToList()
                    .FirstOrDefault();

                if (textObject == null) {
                    continue;
                }

                return textObject;
            }

            throw new Exception($"To much repeats {MaxRepeatFind}");
        }
    }
}