﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.UnityStruct;
using Multiverse.Automation.Questions;
using ScreenPlay;

namespace Multiverse.Automation.Commands {
    public sealed class SwipeAmuletCommand : ICommand {
        private const float SwipeLength = 300f;
        private const float HalfSwipeLength = SwipeLength / 2;
        private const float SwipeDurationSeconds = 1;

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            var centerPosition = actor.AskFor(new CenterScreenPositionQuestion());

            var fromLeft = new AltUnityVector2(centerPosition.x - HalfSwipeLength, centerPosition.y);
            var toRight = new AltUnityVector2(centerPosition.x + HalfSwipeLength, centerPosition.y);

            altUnityDriver.SwipeAndWait(fromLeft, toRight, SwipeDurationSeconds);
        }
    }
}