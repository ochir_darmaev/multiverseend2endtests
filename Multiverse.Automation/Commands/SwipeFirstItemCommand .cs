﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.AltUnity;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using AltUnityTester.AltUnityDriver.UnityStruct;
using Multiverse.Automation.Model.SwipePositions;
using ScreenPlay;
using System;
using System.Linq;

namespace Multiverse.Automation.Commands {
    public sealed class SwipeFirstItemCommand : ICommand {
        private const float DurationSwipeSecond = 0.5f;
        private readonly ISwipePosition swipePosition;
        public SwipeFirstItemCommand(ISwipePosition swipePosition) {
            this.swipePosition = swipePosition;
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();

            var (from, to) = GetSwipePoints(altUnityDriver);

            altUnityDriver.SwipeAndWait(from, to, DurationSwipeSecond);
        }

        private (AltUnityVector2 from, AltUnityVector2 to) GetSwipePoints(AltUnityDriver altUnityDriver) {
            var targetItem = GetTargetItem(altUnityDriver);
            var destinationItem = GetDestinationItem(altUnityDriver, targetItem);

            return (targetItem.getScreenPosition(), destinationItem.getScreenPosition());
        }

        private AltUnityObject GetTargetItem(AltUnityDriver altUnityDriver) {
            var altUnityObjects = altUnityDriver.FindObjects(By.PATH, $"//{swipePosition.ComponentFrom}/*");
            var target = altUnityObjects.FirstOrDefault(x => swipePosition.FromTargetToDestination.Keys.Contains(x.name));

            if (target == null) {
                throw new Exception($"component {swipePosition.ComponentFrom} has not contained mapped items");
            }

            return target;
        }

        private AltUnityObject GetDestinationItem(AltUnityDriver altUnityDriver, AltUnityObject targetItem) {
            var altUnityObjects = altUnityDriver.FindObjects(By.PATH, $"//{swipePosition.ComponentTo}/*");
            var destination = altUnityObjects.Find(x => x.name == swipePosition.FromTargetToDestination[targetItem.name]);

            return destination;
        }
    }
}