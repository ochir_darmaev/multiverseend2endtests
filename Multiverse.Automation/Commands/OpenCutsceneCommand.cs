﻿using AltUnityTester.AltUnityDriver;
using FluentAssertions;
using Multiverse.Automation.Model;
using Newtonsoft.Json;
using ScreenPlay;
using System;

namespace Multiverse.Automation.Commands {
    public sealed class OpenCutsceneCommand : ICommand {
        private readonly int cutsceneId;
        private readonly TimeSpan timeout;
        private readonly int? sceneId;

        public OpenCutsceneCommand(int cutsceneId, TimeSpan timeout, int? sceneId = null) {
            this.cutsceneId = cutsceneId;
            this.timeout = timeout;
            this.sceneId = sceneId;
        }

        public void ExecuteAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            // todo AutomationTest should be encapsulated
            const string paramSeparator = "?";
            var response = altUnityDriver.CallStaticMethods("WhiteSharx.Mars.Automation.AutomationTest", "OpenCutscene", $"{cutsceneId}{paramSeparator}{sceneId}");
            var result = JsonConvert.DeserializeObject<Result>(response);
            result.Success.Should().BeTrue();
            result.ErrorMessage.Should().BeNull();

            altUnityDriver.WaitForCurrentSceneToBe(SceneName.Story.ToString(), timeout.TotalSeconds);
        }
    }
}