﻿using AltUnityTester.AltUnityDriver;
using Multiverse.Automation.Model;
using ScreenPlay;
using System;

namespace Multiverse.Automation.Questions {
    public sealed class CurrentScreenQuestion : IQuestion<bool> {
        private readonly SceneName sceneName;
        private readonly TimeSpan timeout;

        public CurrentScreenQuestion(SceneName sceneName, TimeSpan timeout) {
            this.sceneName = sceneName;
            this.timeout = timeout;
        }

        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForCurrentSceneToBe(sceneName.ToString(), timeout.TotalSeconds);

            return true;
        }
    }
}