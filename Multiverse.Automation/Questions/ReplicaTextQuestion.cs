﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using Multiverse.Automation.Model;
using ScreenPlay;
using System;
using System.Threading;

namespace Multiverse.Automation.Questions {
    public sealed class ReplicaTextQuestion : IQuestion<string> {
        private const int MaxRepeats = 30;
        private static readonly TimeSpan RequestInterval = TimeSpan.FromMilliseconds(100);
        private readonly string nameCharacter;
        private readonly bool isAnybodyCharacter;

        public ReplicaTextQuestion(Character character) {
            isAnybodyCharacter = character == Character.Any;
            nameCharacter = character.ToDescriptionString();
        }

        public string AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();

            if (!isAnybodyCharacter) {
                altUnityDriver.WaitForObjectWithText(By.NAME, "NameText", nameCharacter);
            }

            WaitAllTextDisplayed(altUnityDriver);

            var replicaObj = altUnityDriver.WaitForObject(By.NAME, "ReplicaText");
            return replicaObj.GetText();
        }

        private static void WaitAllTextDisplayed(AltUnityDriver altUnityDriver) {
            const string replicaComponent = "WhiteSharx.Multiverse.Scenes.Story.ReplicaSystem.ReplicaBehaviour";

            var i = 0;
            ReplicaState state;
            do {
                Thread.Sleep(RequestInterval);
                var replicaObject = altUnityDriver.WaitForObject(By.NAME, "Replica(Clone)");
                var result = replicaObject.GetComponentProperty(replicaComponent, "State", "Assembly-CSharp");

                state = (ReplicaState)int.Parse(result);
                if (i++ > MaxRepeats) {
                    throw new Exception($"Too much repeats '{MaxRepeats}'");
                }
            } while (state != ReplicaState.AnimateEnded);
        }
    }
}