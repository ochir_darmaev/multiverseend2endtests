﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using AltUnityTester.AltUnityDriver.UnityStruct;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    internal sealed class CenterScreenPositionQuestion : IQuestion<AltUnityVector2> {
        public AltUnityVector2 AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            const string value = "Behaviours";
            var obj = altUnityDriver.FindObject(By.NAME, value);
            return obj.getScreenPosition();
        }
    }
}