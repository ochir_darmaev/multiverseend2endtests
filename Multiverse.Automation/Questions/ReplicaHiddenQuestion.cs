﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.AltUnity;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class ReplicaHiddenQuestion : IQuestion<bool> {
        private readonly string lastReplica;

        public ReplicaHiddenQuestion(string lastReplica) {
            this.lastReplica = lastReplica;
        }

        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            AltUnityObject objectWithText;
            try {
                objectWithText = altUnityDriver.WaitForObject(By.NAME, "ReplicaText", "", true, 1);
            } catch (NotFoundException) {
                return true;
            } catch (WaitTimeOutException) {
                return true;
            }

            if (lastReplica == objectWithText.GetText()) {
                altUnityDriver.WaitForObjectNotBePresent(By.ID, objectWithText.id.ToString());
            }

            return true;
        }
    }
}