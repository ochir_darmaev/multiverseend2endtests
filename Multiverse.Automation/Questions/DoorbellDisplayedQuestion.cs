﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class DoorbellDisplayedQuestion : IQuestion<bool> {
        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForObject(By.NAME, "DoorHandSplit");
            return true;
        }
    }
}