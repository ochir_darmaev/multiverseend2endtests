﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class CurrentCutsceneQuestion : IQuestion<bool> {
        private readonly int id;

        public CurrentCutsceneQuestion(int id) {
            this.id = id;
        }

        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();

            const double timeoutSec = 10;
            const string component = "Behaviours";
            var sceneId = $"Scene_{id}";
            altUnityDriver.WaitForObject(By.PATH, $"/{component}/{sceneId}", null, true, timeoutSec);

            return true;
        }
    }
}