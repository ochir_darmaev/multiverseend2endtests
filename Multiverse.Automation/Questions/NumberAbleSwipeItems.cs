﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using Multiverse.Automation.Model.SwipePositions;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class NumberAbleSwipeItems : IQuestion<int> {
        private readonly ISwipePosition swipePosition;

        public NumberAbleSwipeItems(ISwipePosition swipePosition) {
            this.swipePosition = swipePosition;
        }

        public int AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            var list = altUnityDriver.FindObjects(By.PATH, $"//{swipePosition.ComponentFrom}/*");

            return list.Count;
        }
    }
}