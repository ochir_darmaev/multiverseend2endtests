﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using Multiverse.Automation.Model;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class ButtonDisplayedQuestion : IQuestion<bool> {
        private readonly string buttonName;

        public ButtonDisplayedQuestion(Button button) {
            buttonName = button.ToDescriptionString();
        }

        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForObject(By.PATH, "//*[contains(@name," + buttonName + ")]");

            return true;
        }
    }
}