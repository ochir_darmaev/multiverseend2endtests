﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class PhoneDisplayedQuestion : IQuestion<bool> {
        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForObject(By.NAME, "Phone");
            return true;
        }
    }
}