﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class PhoneCallPhoneDisplayedQuestion : IQuestion<bool> {
        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForObject(By.NAME, "PhoneCallInteractive(Clone)");

            return true;
        }
    }
}