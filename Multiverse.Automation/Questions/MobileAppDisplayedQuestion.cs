﻿using AltUnityTester.AltUnityDriver;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using Multiverse.Automation.Model;
using ScreenPlay;

namespace Multiverse.Automation.Questions {
    public sealed class MobileAppDisplayedQuestion : IQuestion<bool> {
        private readonly string application;

        public MobileAppDisplayedQuestion(ApplicationButton application) {
            this.application = application.ToDescriptionString();
        }

        public bool AskForAs(IActor actor) {
            var altUnityDriver = actor.GetAbility<AltUnityDriver>();
            altUnityDriver.WaitForObject(By.NAME, application);

            return true;
        }
    }
}