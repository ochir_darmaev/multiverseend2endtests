﻿using System.ComponentModel;

namespace Multiverse.Automation.Model {

    public enum Character {

        [Description("Emily")]
        Emily,

        [Description("Henry")]
        Henry,

        [Description("Butler")]
        Butler,

        [Description("Barista")]
        Barista,

        [Description("Wilson")]
        Wilson,

        Any
    }
}