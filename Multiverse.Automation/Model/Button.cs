﻿using System.ComponentModel;

namespace Multiverse.Automation.Model {

    public enum Button {

        #region Root scene

        [Description("PlayButton")]
        PlayButton,

        #endregion Root scene

        #region Story scene

        [Description("PhoneButton")]
        PhoneButton,

        [Description("TriggerButton")]
        TriggerButton

        #endregion Story scene
    }
}