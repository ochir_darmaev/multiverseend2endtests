﻿using System.Collections.Generic;

namespace Multiverse.Automation.Model.SwipePositions {
    public sealed class BagSwipePosition : ISwipePosition {
        public string ComponentFrom { get; } = "Content";
        public string ComponentTo { get; } = "CompleteSpritesParent";

        public IReadOnlyDictionary<string, string> FromTargetToDestination { get; } = new Dictionary<string, string>
        {
            {"SweaterBlackBrown", "SweaterBlackBrown"},
            {"SweaterLightBrown", "Shirt"},
            {"Passport", "Passport"},
            {"Book", "Book"},
            {"Cosmetic", "Cosmetic"}
        };
    }
}