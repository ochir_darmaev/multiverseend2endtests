﻿using System.Collections.Generic;

namespace Multiverse.Automation.Model.SwipePositions {
    public sealed class FocusCameraSwipePosition : ISwipePosition {
        public string ComponentFrom { get; } = "Trigger_1(Clone)";
        public string ComponentTo { get; } = "Trigger_1(Clone)";

        public IReadOnlyDictionary<string, string> FromTargetToDestination { get; } = new Dictionary<string, string>
        {
            {"CameraFocus", "TargetFocus"},
        };
    }
}