﻿using System.Collections.Generic;

namespace Multiverse.Automation.Model.SwipePositions {
    public sealed class JugSwipePosition : ISwipePosition {
        public string ComponentFrom { get; } = "Content";

        // todo fix name in bundle
        public string ComponentTo { get; } = "CompleteSprtiesParent";

        public IReadOnlyDictionary<string, string> FromTargetToDestination { get; } = new Dictionary<string, string>
        {
            {"Jug", "JugOutline"},
        };
    }
}