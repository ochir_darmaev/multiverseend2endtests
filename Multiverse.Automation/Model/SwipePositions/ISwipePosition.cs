﻿using System.Collections.Generic;

namespace Multiverse.Automation.Model.SwipePositions {
    public interface ISwipePosition {
        string ComponentFrom { get; }
        string ComponentTo { get; }
        IReadOnlyDictionary<string, string> FromTargetToDestination { get; }
    }
}