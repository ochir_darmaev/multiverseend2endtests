﻿using System.ComponentModel;

namespace Multiverse.Automation.Model {

    public enum ApplicationButton {

        [Description("TaxiApplication")]
        TaxiApplication,

        [Description("MessengerApplication")]
        MessengerApplication
    }
}