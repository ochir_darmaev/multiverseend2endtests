﻿using System.ComponentModel;

namespace Multiverse.Automation.Model {

    public enum PhoneMenu {

        #region Main menu

        [Description("MESSENGER")]
        Messenger,

        [Description("FLASHLIGHT")]
        Flashlight,

        [Description("CAMERA")]
        Camera,

        [Description("GALLERY")]
        Gallery,

        [Description("TAXI")]
        Taxi,

        [Description("WI-FI")]
        WiFi,

        [Description("BROWSER")]
        Browser,

        [Description("MAP")]
        Map,

        #endregion Main menu

        #region Taxi app

        [Description("CALL TAXI")]
        CallTaxi,

        #endregion Taxi app
    }
}