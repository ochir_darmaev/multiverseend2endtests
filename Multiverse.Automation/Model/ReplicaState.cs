﻿namespace Multiverse.Automation.Model {

    // Was copied from code base of multiverse
    public enum ReplicaState {
        Animate,
        AnimateText,
        AnimateEnded
    }
}