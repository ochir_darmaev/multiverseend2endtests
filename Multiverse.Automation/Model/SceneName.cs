﻿using System.ComponentModel;

namespace Multiverse.Automation.Model {

    public enum SceneName {

        [Description("Main")]
        Main,

        [Description("Root")]
        Root,

        [Description("Story")]
        Story
    }
}