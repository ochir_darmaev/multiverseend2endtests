﻿namespace Multiverse.Automation.Model {
    public sealed class Result {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}