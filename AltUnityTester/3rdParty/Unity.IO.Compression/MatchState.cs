namespace AltUnityTester._3rdParty.Unity.IO.Compression
{
    internal enum MatchState {
        HasSymbol = 1,
        HasMatch = 2,
        HasSymbolAndMatch = 3
    }
}
