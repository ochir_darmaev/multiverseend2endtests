namespace AltUnityTester._3rdParty.Unity.IO.Compression
{
    public enum CompressionMode {
        Decompress = 0,
        Compress   = 1
    }
}
