﻿@AltUnity @Screenshot @Mapping
Feature: Cutscene playback
	In order to find bugs
	As a QA
	I want to playback the cutscene from start to finish

Scenario: Playback the cutscene 0
	Given I have the main menu
	When I open the cutscene '0'
	Then Scene should be '1'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '2'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '3'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '4'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then  Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '5'
	#
	When I swipe amulet
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '6'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '7'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Scene should be '8'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 1
	Given I have the main menu
	When I open the cutscene '1'
	Then Scene should be '9'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Button 'trigger' is displayed
	#
	Given Button 'trigger' is displayed
	When I press button 'trigger'
	Then Scene should be '10'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '11'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '12'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '13'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given I have '5' items in bag
	When I swipe item in bag
	Then I have '4' items in bag
	#
	Given I have '4' items in bag
	When I swipe item in bag
	Then I have '3' items in bag
	#
	Given I have '3' items in bag
	When I swipe item in bag
	Then I have '2' items in bag
	#
	Given I have '2' items in bag
	When I swipe item in bag
	Then I have '1' items in bag
	#
	Given I have '1' items in bag
	When I swipe item in bag
	Then I have '0' items in bag
	* Scene should be '15'
	#
	Given Scene is '15'
	When Wait '3' animation second
	Then I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 2
	Given I have the main menu
	When I open the cutscene '2'
	Then Scene should be '17'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '18'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '19'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given I have a phone
	When Wait '1' animation second
	* I open mobile app 'taxi'
	Then Mobile 'taxi app' is displayed
	#
	Given Mobile 'taxi app' is displayed
	When I call taxi
	Then Scene should be '20'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '14'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '21'
	#
	Given Scene is '21'
	When Wait '17' animation second
	Then I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 3
	Given I have the main menu
	When I open the cutscene '3'
	Then Scene should be '23'
	#
	Given Scene is '23'
	When I press button 'trigger'
	Given Replica of 'Emily' is displayed
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '24'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '25'
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '26'
	#
	Given Scene is '27'
	When Wait '1' animation second
	Then  Scene should be '27'
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '28'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '29'
	#
	Given Scene is '29'
	When Wait '1' animation second
	Then Scene should be '30'
	#
	Given Scene is '30'
	* Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '31'
	#
	Given Scene is '32'
	When Wait '1' animation second
	Then I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 4
	Given I have the main menu
	When I open the cutscene '4'
	Then Scene should be '33'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '34'
	#
	Given Phone is displayed
	When I press button 'phone'
	Then Replica of 'Henry' is displayed
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '35'
	#
	Given Scene is '35'
	When Wait '1' animation second
	Then Scene should be '36'
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '37'
	#
	Given I have a doorbell
	When I knock on the door 3 times
	Then Scene should be '38'
	#
	Given Replica of 'Butler' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Butler' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '39'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 5
	Given I have the main menu
	When I open the cutscene '5'
	Then Scene should be '40'
	#
	Given Replica of 'Butler' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '41'
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '42'
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '43'
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	#Given
	When I capture photo frame
	Then Button 'trigger' is displayed
	#
	#Given
	When I make photo
	Then Scene should be '44'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '45'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '46'
	#
	Given I have jug to swipe
	When Wait '1.5' animation second
	* I swipe jug
	Then Scene should be '47'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '48'
	#
	Given Replica of 'Henry' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica of 'Wilson' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* I should see main menu

#----------------------------------------------------------------------------------------------
Scenario: Playback the cutscene 6
	Given I have the main menu
	When I open cutscene '6' scene is '49'
	Then Scene should be '49'
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '50'
	#
	Given Scene is '50'
	When Wait '1' animation second
	Then Scene should be '51'
	#
	Given Scene is '51'
	When Wait '11' animation second
	Then Replica of 'Emily' is displayed
	#
	Given Replica of 'Emily' is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given I have a phone
	When Wait '1' animation second
	* I open mobile app 'messenger'
	Then Mobile 'messenger app' is displayed
	#
	Given Mobile 'messenger app' is displayed
	When I press button 'phone'
	Then Replica is displayed
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Mobile 'messenger app' is displayed
	#
	Given Mobile 'messenger app' is displayed
	When I press button 'phone'
	Then Replica is displayed
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Mobile 'messenger app' is displayed
	#
	Given Mobile 'messenger app' is displayed
	When I press button 'phone'
	Then Replica is displayed
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Mobile 'messenger app' is displayed
	#
	Given Mobile 'messenger app' is displayed
	When I press button 'phone'
	Then Replica is displayed
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Mobile 'messenger app' is displayed
	#
	Given Mobile 'messenger app' is displayed
	When I press button 'phone'
	Then Replica is displayed
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '52'
	#
	Given Scene is '52'
	* Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '53'
	#
	Given Scene is '53'
	When Wait '4' animation second
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	* Scene should be '54'
	#
	Given Scene is '54'
	* Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	Given Replica is displayed
	When I tap on the screen
	Then Replica should be hidden
	#
	When Wait '15' animation second
	Then Scene should be '55'
	#
	Given Scene is '55'
	When Wait '1' animation second
	Then Scene should be '56'
	#
	Given Scene is '56'
	When Wait '12' animation second
	Then I should see main menu