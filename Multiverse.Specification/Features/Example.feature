﻿@AltUnity @Ignore @Mapping
Feature: Cutscene playback example
	In order to find bugs
	As a QA
	I want to playback the cutscene from start to finish

Scenario: Playback the cutscene 5 and scene 46
	Given I have the main menu
	When I open cutscene '5' scene is '46'
	Then Scene should be '46'
