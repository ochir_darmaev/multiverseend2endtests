﻿using ScreenPlay;
using TechTalk.SpecFlow;

namespace Multiverse.Specification.StepDefinitions {
    public abstract class StepBase {
        protected ScenarioContext context;

        protected IActor actor {
            get => context.Get<IActor>();
            set => context.Set(value);
        }

        protected StepBase(ScenarioContext context) {
            this.context = context;
        }
    }
}