﻿using FluentAssertions;
using Multiverse.Automation.Commands;
using Multiverse.Automation.Model;
using Multiverse.Automation.Model.SwipePositions;
using Multiverse.Automation.Questions;
using System;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Multiverse.Specification.StepDefinitions {
    [Binding]
    public class CutscenePlaybackSteps : StepBase {
        public CutscenePlaybackSteps(ScenarioContext context) : base(context) {
        }

        #region Input

        [When(@"I tap on the screen")]
        public void WhenITapOnScreen() {
            actor.Execute(new TapCommand());
        }

        [When(@"I capture photo frame")]
        public void WhenICapturePhotoFrame() {
            actor.Execute(new SwipeFirstItemCommand(new FocusCameraSwipePosition()));
        }

        [When(@"I swipe jug")]
        public void WhenISwipeWater() {
            actor.Execute(new SwipeFirstItemCommand(new JugSwipePosition()));
        }

        [Given(@"I have jug to swipe")]
        public void GivenIHaveJugToSwipe() {
            actor.AskFor(new NumberAbleSwipeItems(new JugSwipePosition()))
                .Should().Be(1);
        }

        [When(@"I press button '(.*)'")]
        public void WhenIPressButton(string buttonName) {
            actor.Execute(new TapCommand(context.Get<Button>(buttonName)));
        }

        [When(@"I swipe item in bag")]
        public void WhenISwipeItem() {
            actor.Execute(new TurnOffCameraTemporarilyCommand(() => {
                actor.Execute(new SwipeFirstItemCommand(new BagSwipePosition()));
            }));
        }

        [When(@"I swipe amulet")]
        public void WhenISwipeAmulet() {
            actor.Execute(new SwipeAmuletCommand());
        }

        [Given(@"I have a doorbell")]
        public void GivenIHaveADoorbell() {
            actor.AskFor(new DoorbellDisplayedQuestion())
                .Should().BeTrue();
        }

        [When(@"I knock on the door 3 times")]
        public void WhenIKnockOnTheDoor() {
            var pauseBetweenKnocks = TimeSpan.FromSeconds(1);
            for (int i = 0; i < 3; i++) {
                Thread.Sleep(pauseBetweenKnocks);
                actor.Execute(new TapCommand());
            }
        }

        [When(@"I make photo")]
        public void WhenIMakePhoto() {
            actor.Execute(new TapCommand(Button.TriggerButton));
        }

        [When(@"I open cutscene '(.*)' scene is '(.*)'")]
        public void WhenIOpenTheCutsceneSceneIs(int cutsceneId, int sceneId) {
            actor.Execute(new OpenCutsceneCommand(cutsceneId, TimeSpan.FromSeconds(10), sceneId));
        }

        #endregion Input

        [Given(@"I have the main menu")]
        public void GivenIHaveTheMainMenu() {
            actor.Execute(new OpenScene(SceneName.Main, TimeSpan.FromSeconds(30)));
        }

        [When(@"I open the cutscene '(.*)'")]
        public void WhenIOpenTheCutscene(int id) {
            actor.Execute(new OpenCutsceneCommand(id, TimeSpan.FromSeconds(30)));
        }

        [Given(@"Replica of '(.*)' is displayed")]
        [Then(@"Replica of '(.*)' is displayed")]
        public void GivenReplicaIsDisplayed(string nameCharacter) {
            var replica = actor.AskFor(new ReplicaTextQuestion(context.Get<Character>(nameCharacter)));
            replica.Should().NotBeNullOrEmpty();
            context.Set(replica, "lastReplica");
        }

        [Given(@"Replica is displayed")]
        [Then(@"Replica is displayed")]
        public void GivenReplicaIsDisplayed() {
            var replica = actor.AskFor(new ReplicaTextQuestion(Character.Any));
            replica.Should().NotBeNullOrEmpty();
            context.Set(replica, "lastReplica");
        }

        [Then(@"Scene should be '(.*)'")]
        [Given(@"Scene is '(.*)'")]
        public void ThenSceneIs(int id) {
            actor.AskFor(new CurrentCutsceneQuestion(id))
                .Should().BeTrue();
        }

        [When(@"I open mobile app '(.*)'")]
        public void WhenIOpenMobileApp(string appName) {
            actor.Execute(new OpenMobileAppCommand(context.Get<PhoneMenu>(appName)));
        }

        [When(@"I call taxi")]
        public void WhenICallTaxi() {
            actor.Execute(new OpenMobileAppCommand(PhoneMenu.CallTaxi));
        }

        [Then(@"Replica should be hidden")]
        public void ThenReplicaOfIsHidden() {
            var lastReplica = context.Get<string>("lastReplica");
            actor.AskFor(new ReplicaHiddenQuestion(lastReplica))
                .Should().BeTrue();
        }

        [Then(@"Button '(.*)' is displayed")]
        [Given(@"Button '(.*)' is displayed")]
        public void ThenButtonIsDisplayed(string buttonName) {
            actor.AskFor(new ButtonDisplayedQuestion(context.Get<Button>(buttonName)))
                .Should().BeTrue();
        }

        [Given(@"I have '(.*)' items in bag")]
        public void GivenIHaveItems(int number) {
            actor.AskFor(new NumberAbleSwipeItems(new BagSwipePosition()))
                .Should().Be(number);
        }

        [Then(@"I have '(.*)' items in bag")]
        public void ThenIHaveItems(int number) {
            actor.AskFor(new NumberAbleSwipeItems(new BagSwipePosition()))
                .Should().Be(number);
        }

        [When(@"Wait '(.*)' animation second")]
        public async Task WhenWaitAnimationSecond(double seconds) {
            await Task.Delay(TimeSpan.FromSeconds(seconds));
        }

        [Given(@"I have the start screen")]
        public void GivenIHaveTheStartScreen() {
            actor.Execute(new OpenScene(SceneName.Root, TimeSpan.FromSeconds(10)));
        }

        [Then(@"I should see main menu")]
        public void ThenIShouldSeeMainMenu() {
            actor.AskFor(new CurrentScreenQuestion(SceneName.Main, TimeSpan.FromSeconds(5)))
                .Should().Be(true);
        }

        [Given(@"I have a phone")]
        public void GivenIHaveAPhone() {
            actor.AskFor(new PhoneDisplayedQuestion())
                .Should().Be(true);
        }

        [Then(@"Mobile '(.*)' is displayed")]
        [Given(@"Mobile '(.*)' is displayed")]
        public void MobileAppIsDisplayed(string mobileApplication) {
            actor.AskFor(new MobileAppDisplayedQuestion(context.Get<ApplicationButton>(mobileApplication)))
                .Should().Be(true);
        }

        [Given(@"Phone is displayed")]
        public void GivenPhoneIsDisplayed() {
            actor.AskFor(new PhoneCallPhoneDisplayedQuestion())
                .Should().BeTrue();
        }
    }
}