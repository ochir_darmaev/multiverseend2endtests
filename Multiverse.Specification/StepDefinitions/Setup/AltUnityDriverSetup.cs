﻿using AltUnityTester.AltUnityDriver;
using ScreenPlay;
using System;
using TechTalk.SpecFlow;

namespace Multiverse.Specification.StepDefinitions.Setup {
    [Binding]
    public class AltUnityDriverSetup : StepBase {
        private const string AltUnityTag = "AltUnity";

        private AltUnityDriver altUnityDriver;

        public AltUnityDriverSetup(ScenarioContext context) : base(context) {
        }

        [Before(AltUnityTag)]
        public void Before() {
            actor = new Actor("Player");
            try {
                altUnityDriver = new AltUnityDriver();
            } catch (System.NullReferenceException ex) {
                throw new Exception($"{nameof(AltUnityDriver)} cannot connect to server. Please ensure that 'AltUnityTester' server is running", ex);
            }

            actor.CanUse(altUnityDriver);
        }

        [After(AltUnityTag)]
        public void After() {
            altUnityDriver?.Stop();
        }
    }
}