﻿using Multiverse.Automation.Model;
using TechTalk.SpecFlow;

namespace Multiverse.Specification.StepDefinitions {
    [Binding]
    public class MappingScenarioVariableToTestArguments {
        private ScenarioContext context;
        private const string MappingTag = "Mapping";
        public MappingScenarioVariableToTestArguments(ScenarioContext context) {
            this.context = context;
        }

        [Before(MappingTag)]
        public void Setup() {
            context.Set(Button.PlayButton, "play");
            context.Set(Button.TriggerButton, "trigger");
            context.Set(Button.PhoneButton, "phone");

            context.Set(PhoneMenu.Messenger, "messenger");
            context.Set(PhoneMenu.Taxi, "taxi");
            context.Set(ApplicationButton.MessengerApplication, "messenger app");
            context.Set(ApplicationButton.TaxiApplication, "taxi app");

            context.Set(Character.Emily, "Emily");
            context.Set(Character.Henry, "Henry");
            context.Set(Character.Wilson, "Wilson");
            context.Set(Character.Butler, "Butler");
        }
    }
}