﻿using Multiverse.Automation.Commands;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace Multiverse.Specification.StepDefinitions.Setup {
    [Binding]
    public class SceenshotEachStepSetup : StepBase {
        private const string ScreenshotTag = "Screenshot";
        public SceenshotEachStepSetup(ScenarioContext context) : base(context) {
        }

        [BeforeStep(ScreenshotTag)]
        public void BeforeStepScreenshot() {
            Screenshot();
        }

        [AfterStep(ScreenshotTag)]
        public void AfterStepScreenshot() {
            Screenshot();
        }

        private void Screenshot() {
            var reportPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                "Report",
                ReplaceInvalidChars(context.StepContext.StepInfo.StepInstance.StepContext.FeatureTitle),
                ReplaceInvalidChars(context.ScenarioInfo.Title));

            if (!Directory.Exists(reportPath)) {
                Directory.CreateDirectory(reportPath);
            }

            var fileName = $"{DateTime.Now.ToFileTimeUtc()}_{ReplaceInvalidChars(context.StepContext.StepInfo.Text)}.png";

            actor.Execute(new ScreenshotPNGCommand(Path.Combine(reportPath, fileName)));
        }

        private string ReplaceInvalidChars(string filename) {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }
    }
}