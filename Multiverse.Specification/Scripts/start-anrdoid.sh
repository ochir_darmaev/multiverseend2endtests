#!/bin/bash

# export deviceName="emulator-5554"
export deviceName="4200639ae0c56485"

export apk="C:\Users\TTN\repos\multiverse-app\out.apk"

export pkg_name="com.xten.multiverse"

export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

#ensure adb works
adb devices

adb -s "$deviceName" install -r -d  "$apk"

# start app
adb -s "$deviceName" shell monkey -p "${pkg_name}" -c android.intent.category.LAUNCHER 1

adb -s "$deviceName" forward tcp:13000 tcp:13000