using AltUnityTester.AltUnityDriver.AltUnity;
using AltUnityTester.AltUnityDriver.Commands.FindObjects;
using AltUnityTester.AltUnityDriver.UnityStruct;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AltUnityTester.Tests {
    public class WhenStoryIsPlaying {
        public const float TapThreshold = 0.1f;
        private AltUnityDriver.AltUnityDriver altUnityDriver;

        [OneTimeSetUp]
        public void OneTimeSetUp() {
            altUnityDriver = new AltUnityDriver.AltUnityDriver();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() {
            altUnityDriver.Stop();
        }

        [Test]
        [TestCase("Phone", "TAXI")]
        [TestCase("Phone", "CALL TAXI")]
        public void Should_tap_phone_button(string component, string buttonText) {
            var position = FindTextPosition(component, buttonText);
            altUnityDriver.HoldButton(position, TapThreshold);
        }

        [Test]
        [TestCase("StoryCanvas", "Buy Coffee")]
        public void Should_tap_action_button(string component, string buttonText) {
            var position = FindTextPosition(component, buttonText);
            altUnityDriver.HoldButton(position, TapThreshold);
        }

        public AltUnityVector2 FindTextPosition(string component, string text) {
            var textObjects = altUnityDriver.FindObjectsWhichContain(By.PATH, $"//{component}//Text");
            Assert.AreNotEqual(0, textObjects.Count);

            var textsFounded = textObjects.Where(x => x.GetText() == text).ToList();

            Assert.AreEqual(1, textsFounded.Count);
            var textObject = textsFounded.First();
            return textObject.getScreenPosition();
        }
    }
}