﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ScreenPlay {
    public class Actor : IActor, IName {
        public string Name { get; }

        private readonly IDictionary<Type, object> abilities;

        public Actor(string name) {
            Name = name;
            abilities = new ConcurrentDictionary<Type, object>();
        }

        public void CanUse<T>(T ability) where T : class {
            if (ability == null) {
                throw new ArgumentNullException(nameof(ability));
            }

            abilities.Add(typeof(T), ability);
        }

        public TAbility GetAbility<TAbility>() {
            if (!abilities.TryGetValue(typeof(TAbility), out var ability)) {
                throw new Exception($"Actor '{Name}' does not have ability '{typeof(TAbility)}'. To use register it via method'{nameof(CanUse)}'");
            }

            return (TAbility)ability;
        }

        public TResult AskFor<TResult>(IQuestion<TResult> question) {
            if (question == null) {
                throw new ArgumentNullException(nameof(question));
            }
            return question.AskForAs(this);
        }

        public void Execute(ICommand command) {
            if (command == null) {
                throw new ArgumentNullException(nameof(command));
            }

            command.ExecuteAs(this);
        }
    }
}