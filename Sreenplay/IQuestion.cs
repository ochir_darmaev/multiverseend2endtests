﻿namespace ScreenPlay {
    public interface IQuestion<out TResult> {
        TResult AskForAs(IActor actor);
    }
}