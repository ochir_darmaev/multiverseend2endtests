﻿namespace ScreenPlay {
    public interface ICommand {
        void ExecuteAs(IActor actor);
    }
}