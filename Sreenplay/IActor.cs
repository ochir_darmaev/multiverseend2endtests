﻿namespace ScreenPlay {
    public interface IActor {
        void CanUse<T>(T ability) where T : class;

        TAbility GetAbility<TAbility>();

        void Execute(ICommand command);

        TResult AskFor<TResult>(IQuestion<TResult> question);
    }
}